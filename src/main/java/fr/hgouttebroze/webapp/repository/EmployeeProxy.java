package fr.hgouttebroze.webapp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import fr.hgouttebroze.webapp.CustomProperties;
import fr.hgouttebroze.webapp.model.Employee;
import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@Component
public class EmployeeProxy {
	
	@Autowired
	private CustomProperties props;
	
	private static Logger logger = LoggerFactory.getLogger(EmployeeProxy.class);
	
	/**
	 * GET all employees
	 * @return an iterable of all employees
	 */
	public Iterable<Employee> getEmployees() {
		
		String baseApiUrl = props.getApiUrl();
		String getEmployeesUrl = baseApiUrl + "/employees";
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Iterable<Employee>> response = restTemplate.exchange(
					getEmployeesUrl, 
					HttpMethod.GET,
					null, 
					new ParameterizedTypeReference<Iterable<Employee>>() {}
				);
		
		logger.debug("Get Employees call " + response.getStatusCode().toString());
		
		return response.getBody();
		
	}

}
