package fr.hgouttebroze.webapp.model;

import lombok.Data;

/* 
 * CLASSE de TYPE POJO - modélise un employé 
 */

@Data
public class Employee {

	private Integer id;
	
	private String firstName;
	
	private String lastName;
	
	private String mail;
	
	private String password;
}
